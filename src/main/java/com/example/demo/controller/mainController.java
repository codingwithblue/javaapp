package com.example.demo.controller;

import com.example.demo.repository.javaData.javaDatabase;
import com.example.demo.repository.javaData.javaDatabaseRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class mainController {
    private static Logger logger = LogManager.getLogger(mainController.class);


    @Autowired
    javaDatabaseRepository javaDtabaseRepo;


    @GetMapping("/custom-login")
    public String mainLogin(Model model) throws Exception {
        logger.info("inside mainLogin");
        //throw new Exception("Exception!");
        return "custom-login";
    }

    @GetMapping("/loggedIn")
    public String mainApLoggedIn(Model model){
        logger.info("inside mainApLoggedIn");
        /*
        String queryToExecute = "SELECT [field1]\n" +
                "      ,[field2]\n" +
                "      ,[field3]\n" +
                "  FROM [javaDatabase].[dbo].[frontEndDisplay]";

        logger.info("{}",jdbcTemplate.queryForList(queryToExecute));
*/

        List<javaDatabase> javaDatabaseList = javaDtabaseRepo.selectValues() ;
        model.addAttribute("javaDatabaseList", javaDatabaseList);

        return "loggedIn";
    }

    @GetMapping("/")
    public String mainRootEntry(Model model) {
        logger.info("inside mainLogin");
        return "custom-login";
    }

    @GetMapping("/appLogin")
    public String appLogin(Model model) throws Exception {
        // process logging in
        logger.info("inside appLogin");

        return "loggedIn";
    }

    @GetMapping("/accessDenied")
    public String accessDenied(Model model){
        // process logging in
        return "accessDenied";
    }

}
