package com.example.demo.component;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

@Component
public class BasicAuthenticationProvider implements AuthenticationProvider {
    private static Logger logger = LogManager.getLogger(BasicAuthenticationProvider.class);
    @Override
    public Authentication authenticate(Authentication authentication){
        Authentication authResult = null;

        // insert LDAP authentication and roles checks here...

        // create auth result
        authResult = new UsernamePasswordAuthenticationToken(
                authentication.getPrincipal(),
                authentication.getCredentials(),
                null
        );
        logger.info("The username is " + authentication.getPrincipal() );
        logger.info("The password is " + authentication.getCredentials() );


        return authResult;

    }

    @Override
    public boolean supports(Class<?> authentication){
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}
