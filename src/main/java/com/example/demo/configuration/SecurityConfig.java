package com.example.demo.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http.csrf().disable();

        http.requiresChannel()
                .anyRequest()
                .requiresInsecure()
                .and()
                .authorizeRequests()
                .antMatchers("/custom-login/**").permitAll()
                .antMatchers("/custom-login/").permitAll()
                .antMatchers("/loggedIn/").authenticated()
                .antMatchers("/accessDenied/").permitAll()
                .antMatchers("/appLogin/").permitAll()
                .antMatchers("/loggedIn/**").authenticated()
                .antMatchers("/accessDenied/**").permitAll()
                .antMatchers("/appLogin/**").permitAll()
                .antMatchers("/webjars/**").permitAll()
                .antMatchers("/css/**").permitAll()
                .antMatchers("/js/**").permitAll()
                .antMatchers("/images/**").permitAll()
                .antMatchers("/test-page").authenticated()
                .antMatchers("/").permitAll()
                .and().formLogin()
                .loginPage("/custom-login")
                .loginProcessingUrl("/appLogin")
                .usernameParameter("username")
                .passwordParameter("password")
                .defaultSuccessUrl("/loggedIn")
                .and().logout().logoutUrl("/appLogout")
                .logoutSuccessUrl("/custom-login")
                .and().exceptionHandling()
                .accessDeniedPage("/accessDenied")


                .and().httpBasic();

    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        CorsConfiguration configuration = new CorsConfiguration();
        configuration.setAllowedOrigins(Arrays.asList("*"));
        configuration.setAllowedMethods(Arrays.asList("HEAD","GET","POST", "PUT"));
        configuration.setAllowCredentials(true);
        configuration.setAllowedHeaders(Arrays.asList("Authorization", "Cache-Control", "Content-Type", HttpHeaders.CONTENT_DISPOSITION));
        configuration.addExposedHeader(HttpHeaders.CONTENT_DISPOSITION);
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        source.registerCorsConfiguration("/**", configuration);
        return source;


    }
}
