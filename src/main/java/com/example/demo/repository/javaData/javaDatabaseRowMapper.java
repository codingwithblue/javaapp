package com.example.demo.repository.javaData;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class javaDatabaseRowMapper implements RowMapper<javaDatabase> {

    @Override
    public javaDatabase mapRow(ResultSet rs, int rowNum) throws SQLException {
        javaDatabase javDatabaseObject = new javaDatabase();
        javDatabaseObject.setField1( rs.getString("field1"));
        javDatabaseObject.setField2( rs.getString("field2"));
        javDatabaseObject.setField3( rs.getString("field3"));
        return javDatabaseObject;
    }
}
