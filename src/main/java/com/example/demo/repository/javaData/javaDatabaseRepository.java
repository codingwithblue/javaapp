package com.example.demo.repository.javaData;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class javaDatabaseRepository {

    @Autowired
    private JdbcTemplate jdbctemplate;

    public List<javaDatabase> selectValues() {
        String queryToExecute = "SELECT [field1]\n" +
                "      ,[field2]\n" +
                "      ,[field3]\n" +
                "  FROM [javaDatabase].[dbo].[frontEndDisplay]";

        List<javaDatabase> resultsList = jdbctemplate.query(
                queryToExecute,
                new javaDatabaseRowMapper()
        );

        return resultsList;
    }

}
